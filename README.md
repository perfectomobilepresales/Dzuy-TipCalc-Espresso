# AndroidDemoApp
Simple tip calculator - Can be used for Android related demos. Includes also few Espresso tests.
This project using a share keystore file, so that teh app signature will be the same for all build environment.

***** To run in command line from CI Environment  *****
- For Jenkins CI
    $>gradle assembleDebug assembleAndroidTest perfecto

- For BuddyBuild CI
    $>/gradlew assembleDebug assembleAndroidTest perfecto


