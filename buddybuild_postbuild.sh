#!/usr/bin/env bash

# Install Slather
pwd
ls -al

pwd
#Checking to see which tools are installed...
#ls -al
#java -version
#gradle -version
#mvn -version
#ant -version    :: Is NOT installed by default
#git --version
#brew --version  :: Is NOT installed by default
#curl --version

echo ----- Run EspressoTest on multiple devices in Perfecto CQ LAb -------
# ------------------------------------------------------------------------
# Notes:
#   - Edit the ConfigFile for devices run and login token
#   - Result of Espresso UI Screen captures will be in Perfecto Report Server
#   - For BuddyBuild CI - must use gradlew instead of gradle
#
# ---------------------------------------------------------------------------
./gradlew assembleDebug assembleAndroidTest perfecto





